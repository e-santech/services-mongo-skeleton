#!/usr/bin/groovy

def MAVEN_REPO = './m2repo'
def MAVEN_CONFIG_ID = "maven-settings"
def ENV_DOCKER_HOST = 'tcp://atlas.lab:4243'

pipeline {
    agent any
    options {
        disableConcurrentBuilds()
        timeout(time: 30, unit: 'MINUTES')
        buildDiscarder(logRotator(numToKeepStr         : '5',
                                  artifactNumToKeepStr : '5'
        ))
    }

    environment {
        DOCKER_HOST = ENV_DOCKER_HOST
        DOCKER_CERT_PATH = "" // Clean cert path because it break the maven docker plugin when using a tcp docker_host!
    }

    stages {
        stage('Checkout') {
            steps {
                checkout scm
            }
        }
        stage('Build') {
            steps {
                withMaven(
                        mavenSettingsConfig: "${MAVEN_CONFIG_ID}",
                        mavenLocalRepo: "${MAVEN_REPO}") {
                    sh "mvn compile"
                }
            }
        }
        stage('Test') {
            steps {
                withMaven(
                        mavenSettingsConfig: "${MAVEN_CONFIG_ID}",
                        mavenLocalRepo: "${MAVEN_REPO}") {
                    sh "mvn test"
                }
            }
        }
        stage('Deploy') {
            when {
                branch 'master'
            }
            steps {
                withMaven(
                        mavenSettingsConfig: "${MAVEN_CONFIG_ID}",
                        mavenLocalRepo: "${MAVEN_REPO}") {
                    sh "mvn deploy"
                }
            }
        }
    }

    post {
        always {
            deleteDir()
        }
    }
}
