package fr.santech.fk2.repository.skeleton;

import fr.santech.fk2.domain.skeleton.MyObject;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.UUID;

public interface MyObjectRepository extends MongoRepository<MyObject, UUID> {
}
