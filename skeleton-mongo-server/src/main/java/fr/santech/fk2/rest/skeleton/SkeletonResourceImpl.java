package fr.santech.fk2.rest.skeleton;

import fr.santech.fk2.common.exception.ApiException;
import fr.santech.fk2.common.security.SecurityUtils;
import fr.santech.fk2.domain.skeleton.MyObject;
import fr.santech.fk2.dto.skeleton.MyObjectDTO;
import fr.santech.fk2.mapper.skeleton.MyObjectMapper;
import fr.santech.fk2.repository.skeleton.MyObjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "/api/skeletons")
@Transactional
@Service
public class SkeletonResourceImpl implements SkeletonResource {
    private static Logger log = LoggerFactory.getLogger(SkeletonResourceImpl.class);

    private MyObjectRepository objectRepository;
    private MyObjectMapper objectMapper;

    @Autowired
    public SkeletonResourceImpl(MyObjectRepository objectRepository, MyObjectMapper objectMapper) {
        this.objectRepository = objectRepository;
        this.objectMapper = objectMapper;
    }

    @Override
    public ResponseEntity<MyObjectDTO> getObject(@RequestHeader(value = "Authorization", required = false) String authorizationToken,
                                                 @RequestParam(value = "id") UUID id) throws ApiException {
        String currentUserLogin = SecurityUtils.getCurrentUserLogin();
        UUID currentUserUUID = SecurityUtils.getCurrentUserUUID();

        log.info("Get Networks for current user {}", currentUserLogin);
        MyObject myObject = objectRepository.findOne(id);
        return ResponseEntity.ok().body(objectMapper.myObjectToMyObjectDTO(myObject));
    }
}
