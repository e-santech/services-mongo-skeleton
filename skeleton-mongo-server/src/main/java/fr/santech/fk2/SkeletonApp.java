package fr.santech.fk2;

import fr.santech.fk2.common.configuration.DefaultProfileUtil;
import fr.santech.fk2.skeletons.AbstractMongoDBMicroServiceApp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;
import java.net.InetAddress;

@EnableFeignClients
@EnableConfigurationProperties
public class SkeletonApp extends AbstractMongoDBMicroServiceApp {
    private static final Logger log = LoggerFactory.getLogger(SkeletonApp.class);

    /**
     * Initializes the microservice.
     * <p>
     * Spring profiles can be configured with a program arguments --spring.profiles.active=your-active-profile
     */
    @PostConstruct
    public void initApplication() {
    }

    public static void main(String[] args) throws Exception {
        SpringApplication app = new SpringApplication(SkeletonApp.class);
        DefaultProfileUtil.addDefaultProfile(app, args);
        Environment env = app.run(args).getEnvironment();
        log.info("\n----------------------------------------------------------\n\t" +
                         "Application '{}' is running! Access URLs:\n\t" +
                         "Local: \t\thttp://127.0.0.1:{}\n\t" +
                         "External: \thttp://{}:{}\n----------------------------------------------------------",
                 env.getProperty("spring.application.name"),
                 env.getProperty("server.port"),
                 InetAddress.getLocalHost().getHostAddress(),
                 env.getProperty("server.port"));
    }
}