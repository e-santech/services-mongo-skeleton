package fr.santech.fk2.domain.skeleton;

import fr.santech.fk2.common.entities.BaseMongoEntity;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
public class MyObject extends BaseMongoEntity {
    @Field
    private String field;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}
