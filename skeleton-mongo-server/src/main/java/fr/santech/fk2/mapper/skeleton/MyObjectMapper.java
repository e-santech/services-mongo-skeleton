package fr.santech.fk2.mapper.skeleton;

import fr.santech.fk2.domain.skeleton.MyObject;
import fr.santech.fk2.dto.skeleton.MyObjectDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MyObjectMapper {
    MyObjectDTO myObjectToMyObjectDTO(MyObject myObject);

    MyObject myObjectDTOToMyObject(MyObjectDTO myObjectDTO);

    List<MyObjectDTO> myObjectsToMyObjectDTOs(List<MyObject> myObjects);

    List<MyObject> myObjectDTOsToMyObjects(List<MyObjectDTO> myObjectDTOs);
}
