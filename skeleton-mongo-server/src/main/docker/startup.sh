#!/usr/bin/env sh

echo "--------------------------- STARTING -----------------------------"
echo "Looking for cerebro : "
while [ ! ${ok} ]
do
  echo "" | nc cerebro 8761 && ok=1 || (echo "Sleep 10 seconds to let cerebro start" && sleep 10)
done

if [ "x$XMX" = "x" ]; then
  XMX=512m
fi

echo "Go now with XMX=${XMX}!"
java \
  -DLOG_PATH=/opt/log \
  -Djava.security.egd=file:/dev/./urandom \
  -Xmx${XMX} -Xms${XMX} \
  -jar /opt/*.war --spring.profiles.active=git
