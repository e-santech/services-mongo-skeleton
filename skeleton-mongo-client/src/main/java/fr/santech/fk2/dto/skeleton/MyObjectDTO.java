package fr.santech.fk2.dto.skeleton;

import java.util.UUID;

public class MyObjectDTO {
    private UUID id;
    private UUID ownerId;
    private String field;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(UUID ownerId) {
        this.ownerId = ownerId;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}
