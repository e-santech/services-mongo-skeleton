package fr.santech.fk2.rest.skeleton;

import fr.santech.fk2.common.exception.ApiException;
import fr.santech.fk2.dto.skeleton.MyObjectDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.UUID;

public interface SkeletonResource {
    @RequestMapping(method = RequestMethod.POST, value = "/request")
    ResponseEntity<MyObjectDTO> getObject(@RequestHeader(value = "Authorization", required = false) String authorizationToken,
                                          @RequestParam(value = "id") UUID id) throws ApiException;
}