package fr.santech.fk2.rest.skeleton;

import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name = ServiceID.SERVICE_ID,
        path = "/api/skeletons",
        decode404 = true)
public interface SkeletonClient extends SkeletonResource {
}